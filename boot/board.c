/* Copyright 2007, Google Inc. */
/* Copyright (c) 2009, Code Aurora Forum.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <boot/boot.h>
#include <boot/flash.h>
#include <msm7k/shared.h>

static unsigned apps_flash_start = 551;
ptentry PTABLE[] = {
    {
        .start = 4,
        .length = 40,
        .name = "boot",
    },
    {
        .start = 60,
        .length = 40,
        .name = "cache",
    },
    {
        .start = 100,
        .length = 40,
        .name = "recovery",
    },
    {
        .start = 140,
        .length = 3,
        .name = "splash",
    },
    {
        .start = 143,
        .length = 552,
        .name = "system",
    },
    {
        .start = 695,
        .length = 801,
        .name = "userdata",
    },
    {
        .name = "",
    },
};

const char *board_cmdline(void) 
{
    return "mem=86M console=ttyMSM2,115200n8";
}

unsigned board_machtype(void)
{
     return 1007003;   // 7627 FFA
    //return 1007002;   // 7627 SURF
}

void board_init()
{
    unsigned n;

    /* if we already have partitions from elsewhere,
    ** don't use the hardcoded ones
    */

    if(smem_ptable_get_apps_flash_start() != 0xFFFFFFFF) {
        apps_flash_start = smem_ptable_get_apps_flash_start();
    }

    if(flash_get_ptn_count() == 0) {
        for(n = 0; PTABLE[n].name[0]; n++) {
            PTABLE[n].start += apps_flash_start;
            flash_add_ptn(PTABLE + n);
        }
    }

    // add modem partition
    ptentry* modem_ptentry = smem_ptable_get_modem_ptentry();
    if (modem_ptentry) {
        flash_add_ptn(modem_ptentry);
    }
 

    // TODO - disable UART init on 7x27 due to issues
    //        with modem on 1004 build
    //clock_enable(UART3_CLK);
    //clock_set_rate(UART3_CLK, 19200000 / 4);

    //uart_init(2);
}

void board_usb_init(void)
{
}

void board_ulpi_init(void)
{
}

void board_reboot(void)
{
    reboot();
}

void board_getvar(const char *name, char *value)
{
}
